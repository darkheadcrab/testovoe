using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    [SerializeField] Animator _animator;
    
    public void Run()
    {
        _animator.SetBool("isRunning", true);
    }

    public void Idle()
    {
        _animator.SetBool("isRunning", false);
    }
}
