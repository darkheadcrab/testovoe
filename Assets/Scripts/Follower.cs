using UnityEngine;

public class Follower : MonoBehaviour
{
    [SerializeField] private GameObject _objectToFollow;
    private Vector3 _offset;
    private void Start() {
        _offset = transform.position - _objectToFollow.transform.position;
    }
    void Update()
    {
        transform.position =(_objectToFollow.transform.position.z * Vector3.forward) + _offset;
    }
}
