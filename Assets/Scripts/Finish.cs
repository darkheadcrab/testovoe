using UnityEngine;
using UnityEngine.Events;

public class Finish : MonoBehaviour
{
    [SerializeField] private UnityEvent _onFinishReached;
    

    private void OnTriggerEnter(Collider other) 
    {
        if(other.TryGetComponent<Player>(out Player player))
        {
            player.Finish();
            _onFinishReached?.Invoke();
        }  
    } 
}
