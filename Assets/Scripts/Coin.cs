using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particles;

    void Update()
    {
        transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider collider)
    {
         if(collider.TryGetComponent<Wallet>(out Wallet wallet))
        {
            Instantiate(_particles, transform.position + Vector3.up, Quaternion.identity);
            wallet.AddCoin();
            Object.Destroy(gameObject);
        }
    }
}
