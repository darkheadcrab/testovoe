
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restarter : MonoBehaviour
{
    public void Restart()
    { 
        SceneManager.LoadScene("Assets/Scenes/Runner.unity");
         
    }
}
