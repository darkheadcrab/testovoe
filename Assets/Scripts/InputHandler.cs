using UnityEngine;

[RequireComponent(typeof(Player))]

public class InputHandler : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    private Player _player;
    private Vector3 _lastPlayerPosition;
    private Vector3 _touchOffset;

    void Start()
    {
        _player = GetComponent<Player>();
    }

    void Update()
    {
        HandleInput();
    }

    public void HandleInput()
    {
        Vector3 target;

        if (Input.GetMouseButton(0))
        {
            var distanceFromCamera = Vector3.Distance(_camera.transform.position, transform.position);
            var touchPositionInWorld = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceFromCamera));

            if (Input.GetMouseButtonDown(0))
            {
                _lastPlayerPosition = transform.position;
                _touchOffset = touchPositionInWorld - _lastPlayerPosition;
            }
            target = new Vector3(touchPositionInWorld.x - _touchOffset.x, transform.position.y, transform.position.z + 2f);
        }
        else target = transform.position;

        _player.MovePlayerTowardsPosition(target);
    }
}
