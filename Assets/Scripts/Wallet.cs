using UnityEngine;
using UnityEngine.UI;

public class Wallet : MonoBehaviour
{
    [SerializeField] private Text _scoreText;

    private int _coins;

    public void AddCoin()
    {       
        _coins++;
        _scoreText.text = _coins.ToString(); 
    }
}
