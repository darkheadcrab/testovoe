using UnityEngine;

[RequireComponent(typeof(InputHandler))]
[RequireComponent(typeof(AnimationHandler))]

public class Player : MonoBehaviour
{
    [SerializeField] private float _runSpeed;
    [SerializeField] private float _moveSpeed;

    private AnimationHandler _animationHandler;
    private InputHandler _inputHandler;

    void Start()
    {
        _inputHandler = GetComponent<InputHandler>();
        _animationHandler = GetComponent<AnimationHandler>();
    }

    public void MovePlayerTowardsPosition(Vector3 target)
    {
        if (transform.position != target)
        {
            var moveX = Mathf.Lerp(transform.position.x, target.x, Time.deltaTime * _moveSpeed);
            var moveZ = Mathf.Lerp(transform.position.z, target.z, Time.deltaTime * _runSpeed);
            transform.position = new Vector3(moveX, transform.position.y, moveZ);
            transform.LookAt(target);
            _animationHandler.Run();
        }
        else _animationHandler.Idle();
    }

    public void Finish()
    {
        _inputHandler.enabled = false;
        _animationHandler.Idle();
        _animationHandler.enabled = false;
    }
}
